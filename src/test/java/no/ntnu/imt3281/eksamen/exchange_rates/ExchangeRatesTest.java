package no.ntnu.imt3281.eksamen.exchange_rates;

import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.Assert.*;

public class ExchangeRatesTest {
    private final static String JSON = "{\"header\":{\"id\":\"IREF414229\",\"prepared\":\"2019-10-22T10:50:21\",\"test\":false,\"sender\":{\"id\":\"NB\"},\"receiver\":{\"id\":\"ANONYMOUS\"}},\"dataSets\":[{\"action\":\"Information\",\"series\":{\"0:0:0:0\":{\"attributes\":[0,0,0,0],\"observations\":{\"0\":[\"6.1256\"],\"1\":[\"6.1362\"],\"2\":[\"6.1118\"],\"3\":[\"6.1116\"],\"4\":[\"6.1474\"],\"5\":[\"6.1498\"]}},\"0:1:0:0\":{\"attributes\":[1,0,1,0],\"observations\":{\"0\":[\"506.98\"],\"1\":[\"505.95\"],\"2\":[\"508.55\"],\"3\":[\"510.94\"],\"4\":[\"512.14\"],\"5\":[\"510.87\"]}},\"0:2:0:0\":{\"attributes\":[0,0,0,0],\"observations\":{\"0\":[\"2.1796\"],\"1\":[\"2.185\"],\"2\":[\"2.1916\"],\"3\":[\"2.1955\"],\"4\":[\"2.2212\"],\"5\":[\"2.2339\"]}},\"0:3:0:0\":{\"attributes\":[0,0,0,0],\"observations\":{\"0\":[\"6.8416\"],\"1\":[\"6.8594\"],\"2\":[\"6.8756\"],\"3\":[\"6.9074\"],\"4\":[\"6.8578\"],\"5\":[\"6.8379\"]}},\"0:4:0:0\":{\"attributes\":[1,0,1,0],\"observations\":{\"0\":[\"913.03\"],\"1\":[\"912.26\"],\"2\":[\"912\"],\"3\":[\"913.44\"],\"4\":[\"914.16\"],\"5\":[\"915.56\"]}},\"0:5:0:0\":{\"attributes\":[1,0,1,0],\"observations\":{\"0\":[\"127.21\"],\"1\":[\"127.22\"],\"2\":[\"127.68\"],\"3\":[\"127.96\"],\"4\":[\"127.95\"],\"5\":[\"127.29\"]}},\"0:6:0:0\":{\"attributes\":[2,0,1,0],\"observations\":{\"0\":[\"38.37\"],\"1\":[\"38.33\"],\"2\":[\"38.641\"],\"3\":[\"38.824\"],\"4\":[\"38.916\"],\"5\":[\"38.816\"]}},\"0:7:0:0\":{\"attributes\":[1,0,1,0],\"observations\":{\"0\":[\"132.81\"],\"1\":[\"132.53\"],\"2\":[\"133.23\"],\"3\":[\"133.84\"],\"4\":[\"134.17\"],\"5\":[\"133.82\"]}},\"0:8:0:0\":{\"attributes\":[0,0,0,0],\"observations\":{\"0\":[\"9.9155\"],\"1\":[\"9.8953\"],\"2\":[\"9.9463\"],\"3\":[\"9.993\"],\"4\":[\"10.0165\"],\"5\":[\"9.9915\"]}},\"0:9:0:0\":{\"attributes\":[0,0,0,0],\"observations\":{\"0\":[\"11.1689\"],\"1\":[\"11.1719\"],\"2\":[\"11.1813\"],\"3\":[\"11.2319\"],\"4\":[\"11.2811\"],\"5\":[\"11.2207\"]}},\"0:10:0:0\":{\"attributes\":[0,0,0,0],\"observations\":{\"0\":[\"1.1566\"],\"1\":[\"1.1591\"],\"2\":[\"1.164\"],\"3\":[\"1.1665\"],\"4\":[\"1.1663\"],\"5\":[\"1.1605\"]}},\"0:11:0:0\":{\"attributes\":[1,0,1,0],\"observations\":{\"0\":[\"133.84\"],\"1\":[\"133.52\"],\"2\":[\"134.21\"],\"3\":[\"134.77\"],\"4\":[\"135.03\"],\"5\":[\"134.63\"]}},\"0:12:0:0\":{\"attributes\":[0,0,1,0],\"observations\":{\"0\":[\"2.9551\"],\"1\":[\"2.9553\"],\"2\":[\"2.9709\"],\"3\":[\"2.9915\"],\"4\":[\"3.0078\"],\"5\":[\"3.0026\"]}},\"0:13:0:0\":{\"attributes\":[1,1,0,0],\"observations\":{\"0\":[\"108.38\"],\"1\":[\"108.31\"],\"2\":[\"108.6\"],\"3\":[\"109.01\"],\"4\":[\"109.23\"],\"5\":[\"108.96\"]}},\"0:14:0:0\":{\"attributes\":[3,0,1,0],\"observations\":{\"0\":[\"0.064015\"],\"1\":[\"0.064019\"],\"2\":[\"0.064146\"],\"3\":[\"0.064401\"],\"4\":[\"0.064538\"],\"5\":[\"0.064331\"]}},\"0:15:0:0\":{\"attributes\":[0,0,0,0],\"observations\":{\"0\":[\"2.6028\"],\"1\":[\"2.6125\"],\"2\":[\"2.6239\"],\"3\":[\"2.6214\"],\"4\":[\"2.6165\"],\"5\":[\"2.6119\"]}},\"0:16:0:0\":{\"attributes\":[2,0,1,0],\"observations\":{\"0\":[\"12.866\"],\"1\":[\"12.824\"],\"2\":[\"12.835\"],\"3\":[\"12.84\"],\"4\":[\"12.889\"],\"5\":[\"12.836\"]}},\"0:17:0:0\":{\"attributes\":[0,0,1,0],\"observations\":{\"0\":[\"8.388\"],\"1\":[\"8.4151\"],\"2\":[\"8.4291\"],\"3\":[\"8.5069\"],\"4\":[\"8.5487\"],\"5\":[\"8.523\"]}},\"0:18:0:0\":{\"attributes\":[0,0,1,0],\"observations\":{\"0\":[\"0.7566\"],\"1\":[\"0.7584\"],\"2\":[\"0.7589\"],\"3\":[\"0.7581\"],\"4\":[\"0.7601\"],\"5\":[\"0.7614\"]}},\"0:19:0:0\":{\"attributes\":[1,0,1,0],\"observations\":{\"0\":[\"46.2\"],\"1\":[\"46.13\"],\"2\":[\"46.14\"],\"3\":[\"46.19\"],\"4\":[\"46.31\"],\"5\":[\"46.45\"]}},\"0:20:0:0\":{\"attributes\":[0,0,0,0],\"observations\":{\"0\":[\"2.1651\"],\"1\":[\"2.1704\"],\"2\":[\"2.1759\"],\"3\":[\"2.1807\"],\"4\":[\"2.1843\"],\"5\":[\"2.1743\"]}},\"0:21:0:0\":{\"attributes\":[0,0,0,0],\"observations\":{\"0\":[\"5.7025\"],\"1\":[\"5.6951\"],\"2\":[\"5.6745\"],\"3\":[\"5.7064\"],\"4\":[\"5.7497\"],\"5\":[\"5.7588\"]}},\"0:22:0:0\":{\"attributes\":[2,0,1,0],\"observations\":{\"0\":[\"17.477\"],\"1\":[\"17.497\"],\"2\":[\"17.57\"],\"3\":[\"17.591\"],\"4\":[\"17.654\"],\"5\":[\"17.587\"]}},\"0:23:0:0\":{\"attributes\":[2,0,1,0],\"observations\":{\"0\":[\"5.781\"],\"1\":[\"5.797\"],\"2\":[\"5.82\"],\"3\":[\"5.835\"],\"4\":[\"5.833\"],\"5\":[\"5.797\"]}},\"0:24:0:0\":{\"attributes\":[0,0,0,0],\"observations\":{\"0\":[\"2.2605\"],\"1\":[\"2.2601\"],\"2\":[\"2.2722\"],\"3\":[\"2.2868\"],\"4\":[\"2.3079\"],\"5\":[\"2.3104\"]}},\"0:25:0:0\":{\"attributes\":[1,0,1,0],\"observations\":{\"0\":[\"208.68\"],\"1\":[\"208.34\"],\"2\":[\"209.35\"],\"3\":[\"210.42\"],\"4\":[\"211.01\"],\"5\":[\"210.44\"]}},\"0:26:0:0\":{\"attributes\":[2,0,1,0],\"observations\":{\"0\":[\"14.09\"],\"1\":[\"13.985\"],\"2\":[\"13.993\"],\"3\":[\"14.032\"],\"4\":[\"14.053\"],\"5\":[\"14.044\"]}},\"0:27:0:0\":{\"attributes\":[1,0,1,0],\"observations\":{\"0\":[\"92.66\"],\"1\":[\"92.52\"],\"2\":[\"92.06\"],\"3\":[\"92.43\"],\"4\":[\"92.46\"],\"5\":[\"92.42\"]}},\"0:28:0:0\":{\"attributes\":[0,0,0,0],\"observations\":{\"0\":[\"6.5666\"],\"1\":[\"6.5706\"],\"2\":[\"6.5835\"],\"3\":[\"6.6017\"],\"4\":[\"6.6212\"],\"5\":[\"6.5998\"]}},\"0:29:0:0\":{\"attributes\":[2,0,1,0],\"observations\":{\"0\":[\"29.59\"],\"1\":[\"29.702\"],\"2\":[\"29.753\"],\"3\":[\"29.867\"],\"4\":[\"29.935\"],\"5\":[\"29.882\"]}},\"0:30:0:0\":{\"attributes\":[1,0,1,0],\"observations\":{\"0\":[\"160.2\"],\"1\":[\"160.92\"],\"2\":[\"160.48\"],\"3\":[\"159.65\"],\"4\":[\"160.4\"],\"5\":[\"159.85\"]}},\"0:31:0:0\":{\"attributes\":[2,0,1,0],\"observations\":{\"0\":[\"29.235\"],\"1\":[\"29.262\"],\"2\":[\"29.369\"],\"3\":[\"29.432\"],\"4\":[\"29.503\"],\"5\":[\"29.464\"]}},\"0:32:0:0\":{\"attributes\":[1,1,0,0],\"observations\":{\"0\":[\"117.44\"],\"1\":[\"117.33\"],\"2\":[\"117.66\"],\"3\":[\"118.13\"],\"4\":[\"118.4\"],\"5\":[\"118.1\"]}},\"0:33:0:0\":{\"attributes\":[0,0,0,0],\"observations\":{\"0\":[\"9.0677\"],\"1\":[\"9.0874\"],\"2\":[\"9.1267\"],\"3\":[\"9.1469\"],\"4\":[\"9.1467\"],\"5\":[\"9.1006\"]}},\"0:34:0:0\":{\"attributes\":[4,1,0,0],\"observations\":{\"0\":[\"12.43322\"],\"1\":[\"12.4418\"],\"2\":[\"12.4923\"],\"3\":[\"12.54062\"],\"4\":[\"12.56026\"],\"5\":[\"12.50963\"]}},\"0:35:0:0\":{\"attributes\":[0,0,0,0],\"observations\":{\"0\":[\"0.6016\"],\"1\":[\"0.5976\"],\"2\":[\"0.5967\"],\"3\":[\"0.5997\"],\"4\":[\"0.6031\"],\"5\":[\"0.6003\"]}},\"0:36:0:0\":{\"attributes\":[0,0,0,0],\"observations\":{\"0\":[\"4.3751\"],\"1\":[\"4.3719\"],\"2\":[\"4.3811\"],\"3\":[\"4.3852\"],\"4\":[\"4.3953\"],\"5\":[\"4.3919\"]}},\"0:37:0:0\":{\"attributes\":[1,0,1,0],\"observations\":{\"0\":[\"10.72\"],\"1\":[\"10.75\"],\"2\":[\"10.8\"],\"3\":[\"10.83\"],\"4\":[\"10.83\"],\"5\":[\"10.76\"]}},\"0:38:0:0\":{\"attributes\":[0,0,1,0],\"observations\":{\"0\":[\"0.5919\"],\"1\":[\"0.5932\"],\"2\":[\"0.5957\"],\"3\":[\"0.5969\"],\"4\":[\"0.5973\"],\"5\":[\"0.5936\"]}},\"0:39:0:0\":{\"attributes\":[1,0,1,0],\"observations\":{\"0\":[\"7.35\"],\"1\":[\"7.32\"],\"2\":[\"7.35\"],\"3\":[\"7.37\"],\"4\":[\"7.38\"],\"5\":[\"7.36\"]}}}}],\"structure\":{\"name\":\"Valutakurser\",\"description\":\"Norges Banks valutakurser\",\"dimensions\":{\"dataset\":[],\"series\":[{\"id\":\"FREQ\",\"name\":\"Frekvens\",\"description\":\"Tidsintervallet for observasjoner over en gitt tidsperiode.\",\"keyPosition\":0,\"role\":null,\"values\":[{\"position\":3,\"id\":\"B\",\"name\":\"Virkedag\"}]},{\"id\":\"BASE_CUR\",\"name\":\"Basisvaluta\",\"description\":\"Første valuta i et valutakvoteringspar. Også kalt transaksjonsvalutaen.\",\"keyPosition\":1,\"role\":null,\"values\":[{\"position\":8,\"id\":\"AUD\",\"name\":\"Australske dollar\"},{\"position\":11,\"id\":\"BGN\",\"name\":\"Bulgarske lev\"},{\"position\":9,\"id\":\"BRL\",\"name\":\"Brasilianske real\"},{\"position\":18,\"id\":\"CAD\",\"name\":\"Kanadiske dollar\"},{\"position\":2,\"id\":\"CHF\",\"name\":\"Sveitsiske franc\"},{\"position\":19,\"id\":\"CNY\",\"name\":\"Kinesiske yuan\"},{\"position\":22,\"id\":\"CZK\",\"name\":\"Tsjekkiske koruna\"},{\"position\":6,\"id\":\"DKK\",\"name\":\"Danske kroner\"},{\"position\":4,\"id\":\"EUR\",\"name\":\"Euro\"},{\"position\":10,\"id\":\"GBP\",\"name\":\"Britiske pund\"},{\"position\":13,\"id\":\"HKD\",\"name\":\"Hong Kong dollar\"},{\"position\":20,\"id\":\"HRK\",\"name\":\"Kroatiske kuna\"},{\"position\":23,\"id\":\"HUF\",\"name\":\"Ungarske forinter\"},{\"position\":5,\"id\":\"I44\",\"name\":\"Importveid kursindeks\"},{\"position\":15,\"id\":\"IDR\",\"name\":\"Indonesiske rupiah\"},{\"position\":16,\"id\":\"ILS\",\"name\":\"Ny israelsk shekel\"},{\"position\":24,\"id\":\"INR\",\"name\":\"Indiske rupi\"},{\"position\":17,\"id\":\"JPY\",\"name\":\"Japanske yen\"},{\"position\":25,\"id\":\"KRW\",\"name\":\"Sørkoreanske won\"},{\"position\":21,\"id\":\"MXN\",\"name\":\"Meksikanske peso\"},{\"position\":27,\"id\":\"MYR\",\"name\":\"Malaysiske ringgit\"},{\"position\":28,\"id\":\"NZD\",\"name\":\"New Zealand dollar\"},{\"position\":12,\"id\":\"PHP\",\"name\":\"Filippinske peso\"},{\"position\":29,\"id\":\"PKR\",\"name\":\"Pakistanske rupi\"},{\"position\":30,\"id\":\"PLN\",\"name\":\"Polske zloty\"},{\"position\":31,\"id\":\"RON\",\"name\":\"Ny rumenske leu\"},{\"position\":32,\"id\":\"RUB\",\"name\":\"Russiske rubler\"},{\"position\":33,\"id\":\"SEK\",\"name\":\"Svenske kroner\"},{\"position\":34,\"id\":\"SGD\",\"name\":\"Singapore dollar\"},{\"position\":35,\"id\":\"THB\",\"name\":\"Thailandske baht\"},{\"position\":36,\"id\":\"TRY\",\"name\":\"Tyrkiske lira\"},{\"position\":37,\"id\":\"TWD\",\"name\":\"Nye taiwanske dollar\"},{\"position\":38,\"id\":\"TWI\",\"name\":\"Industriens effektive valutakurs\"},{\"position\":3,\"id\":\"USD\",\"name\":\"Amerikanske dollar\"},{\"position\":14,\"id\":\"XDR\",\"name\":\"IMF Spesielle trekkrettigheter\"},{\"position\":39,\"id\":\"ZAR\",\"name\":\"Sørafrikanske rand\"},{\"position\":40,\"id\":\"BYN\",\"name\":\"Nye hviterussiske rubler\"},{\"position\":42,\"id\":\"BDT\",\"name\":\"Bangladeshi taka\"},{\"position\":43,\"id\":\"MMK\",\"name\":\"Myanmar kyat\"},{\"position\":7,\"id\":\"ISK\",\"name\":\"Islandske kroner\"}]},{\"id\":\"QUOTE_CUR\",\"name\":\"Kvoteringsvaluta\",\"description\":\"Den andre valutaen i et valutakvoteringspar. Også kjent som motvaluta.\",\"keyPosition\":2,\"role\":null,\"values\":[{\"position\":1,\"id\":\"NOK\",\"name\":\"Norske kroner\"}]},{\"id\":\"TENOR\",\"name\":\"Løpetid\",\"description\":\"Mengde tid igjen inntil tilbakebetaling av et lån eller en finansiell kontrakt utløper.\",\"keyPosition\":3,\"role\":null,\"values\":[{\"position\":2,\"id\":\"SP\",\"name\":\"Spot\"}]}],\"observation\":[{\"id\":\"TIME_PERIOD\",\"name\":\"Tidsperiode\",\"description\":\"Tidsperioden eller tidspunktet for den målte observasjonen.\",\"keyPosition\":4,\"role\":\"time\",\"values\":[{\"start\":\"2019-09-27T00:00:00\",\"end\":\"2019-09-27T23:59:59\",\"id\":\"2019-09-27\",\"name\":\"2019-09-27\"},{\"start\":\"2019-09-30T00:00:00\",\"end\":\"2019-09-30T23:59:59\",\"id\":\"2019-09-30\",\"name\":\"2019-09-30\"},{\"start\":\"2019-10-01T00:00:00\",\"end\":\"2019-10-01T23:59:59\",\"id\":\"2019-10-01\",\"name\":\"2019-10-01\"},{\"start\":\"2019-10-02T00:00:00\",\"end\":\"2019-10-02T23:59:59\",\"id\":\"2019-10-02\",\"name\":\"2019-10-02\"},{\"start\":\"2019-10-03T00:00:00\",\"end\":\"2019-10-03T23:59:59\",\"id\":\"2019-10-03\",\"name\":\"2019-10-03\"},{\"start\":\"2019-10-04T00:00:00\",\"end\":\"2019-10-04T23:59:59\",\"id\":\"2019-10-04\",\"name\":\"2019-10-04\"}]}]},\"attributes\":{\"dataset\":[],\"series\":[{\"id\":\"DECIMALS\",\"name\":\"Desimaler\",\"description\":\"Antall sifre til høyre for desimalskilletegnet.\",\"role\":null,\"values\":[{\"id\":\"4\",\"name\":\"4\"},{\"id\":\"2\",\"name\":\"2\"},{\"id\":\"3\",\"name\":\"3\"},{\"id\":\"6\",\"name\":\"6\"},{\"id\":\"5\",\"name\":\"5\"}]},{\"id\":\"CALCULATED\",\"name\":\"Kalkulert\",\"description\":\"Indikerer om verdien er en kalkulasjon / anslag eller en observert verdi.\",\"role\":null,\"values\":[{\"id\":\"false\",\"name\":\"false\"},{\"id\":\"true\",\"name\":\"true\"}]},{\"id\":\"UNIT_MULT\",\"name\":\"Multiplikator\",\"description\":\"Eksponent i tiende potens slik at en multiplikasjon av observasjonsverdien med 10^UNIT_MULT gir verdien av en enhet.\",\"role\":null,\"values\":[{\"position\":1,\"id\":\"0\",\"name\":\"Enheter\"},{\"position\":3,\"id\":\"2\",\"name\":\"Hundre\"}]},{\"id\":\"COLLECTION\",\"name\":\"Innsamlingstidspunkt\",\"description\":\"Datoer eller perioder for når verdien ble innhentet.\",\"role\":null,\"values\":[{\"position\":2,\"id\":\"C\",\"name\":\"ECB concertation tidspunkt 14:15 CET\"}]}],\"observation\":[]}}}";

    /**
     * Create a new ExchangeRates object using predefined JSON data and check that basic data is initialized.
     * Uses the currencyExists method from ExchangeRates and checks that the currency USD exists and that the
     * currency NOK does not exists.
     */
    @Test
    public void testJSONConstructor() {
        ExchangeRates rates = new ExchangeRates(JSON);
        assertTrue(rates.currencyExists("USD"));
        assertFalse(rates.currencyExists("NOK"));
    }

    /**
     * The JSON data contains a description for each three letter acronym for all the currencies, check
     * that this is correct for USD and SEK. Assume that if those two are correct, the rest is also correct.
     */
    @Test
    public void testGetCurrencyDescription() {
        ExchangeRates rates = new ExchangeRates(JSON);
        assertEquals("Amerikanske dollar", rates.getCurrencyDescription("USD"));
        assertEquals("Svenske kroner", rates.getCurrencyDescription("SEK"));
    }

    /**
     * The dates at which observations has been made and the actual observations of exchange rates is separated in
     * the JSON file so there is one function to get the dates and one to get observations for a specific currency.
     * Both methods is tested here, getDates should return an array of Date objects with the dates for the observations.
     * The getExchangeRates(String) should return the observations (exchange rate) for the given currency on those dates.
     */
    @Test
    public void testGetExchangeRates() {
        ExchangeRates rates = new ExchangeRates(JSON);
        try {
            assertEquals(new SimpleDateFormat("yyyy-MM-dd").parse("2019-09-27"), rates.getDates()[0]);
            assertEquals(new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-03"), rates.getDates()[4]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assertEquals(9.0677, rates.getExchangeRates("USD")[0], 0.0001);
        assertEquals(9.1467, rates.getExchangeRates("USD")[4], 0.0001);
        assertEquals(92.66, rates.getExchangeRates("SEK")[0], 0.0001);
    }

    /**
     * Test exchange currency using latest recorded exchange rate.
     * Tests both exchangeTo and exchangeFrom.
     */
    @Test
    public void testExchangeMoney() {
        ExchangeRates rates = new ExchangeRates(JSON);
        assertEquals(10.98828648660528, rates.exchangeTo(100, "USD"), 0.00001);
        assertEquals(108.20168794633196, rates.exchangeTo(100, "SEK"), 0.00001);
        assertEquals(910.06, rates.exchangeFrom(100, "USD"), 0.00001);
        assertEquals(92.42, rates.exchangeFrom(100, "SEK"), 0.00001);
    }

    /**
     * Tests the function to get all registered currencies in the JSON String. Checks that the first currency is
     * correct and that the correct number of currencies has been extracted from the JSON data.
     */
    @Test
    public void testGetCurrencies() {
        ExchangeRates rates = new ExchangeRates(JSON);
        assertEquals("AUD", rates.getCurrencies()[0]);
        assertEquals(40, rates.getCurrencies().length);
    }

    /**
     * Checks that when a new ExchangeRates object is created from an URL the content of the URL is read
     * and then parsed correctly. As of october 22. 2019 there was 40 registered currencies so we assume
     * the number will stay above 30 and also that USD will keep being registered as a currency.
     */
    @Test
    public void testURLConstructor() {
        try {
            ExchangeRates rates = new ExchangeRates(new URL("https://data.norges-bank.no/api/data/EXR/B..NOK.SP?startPeriod=2019-09-27&endPeriod=2019-10-04&format=sdmx-json&locale=no"));
            assertTrue(rates.currencyExists("USD"));
            assertTrue(rates.getCurrencies().length>30);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}