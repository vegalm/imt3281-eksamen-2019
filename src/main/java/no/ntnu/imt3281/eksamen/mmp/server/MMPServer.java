package no.ntnu.imt3281.eksamen.mmp.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.LinkedList;

/**
 * A fully functioning server for a MMP network. Clients can connect to the server and subscribe to channels/topics
 * and receive messages sent to that channel. Clients can also publish messages to channels (both subscribed and
 * unsubscribed.)
 */
public class MMPServer {
    /**
     * Start the MMPServer, if the passed parameter is true the server is to require username/password
     * to accept connections otherwise username/password is not require.
     *
     * @param requireUnamePwd true if the server should require username/password, false means no username/password is required.
     */
    public MMPServer(boolean requireUnamePwd) {
    }

    /**
     * Reads the connect message from the client and checks the protocol and version number. If the protocol and
     * version information is correct the username/password is checked (if the server is started in that mode).
     * If all information is correct the client is added to the list of clients, and a connected: OK message
     * is sent to the client.
     * If any information is incorrect, a message informing the client about the error is sent back and the
     * client is NOT added to the list of client.
     *
     * @param client a Client object which holds the reader/writer for communicating with the client.
     */
    public void addClient(Client client) {
    }

    /**
     * A class used to represent a single client on the server. Contains the reader and writer used to communicate
     * with the client as well as a list of channels/topics that this client is subscribed to.
     */
    public static class Client {
        BufferedWriter bw = null;
        BufferedReader br = null;
        LinkedList<String> channels = new LinkedList<>();

        /**
         * Creates a new Client object, takes the reader and writer used to communicate with the client
         * as parameters and stores them in the object.
         *
         * @param bw the writer used to send messages to the client
         * @param br the reader used to receive message from the client
         */
        public Client (BufferedWriter bw, BufferedReader br) {
            this.br = br;
            this.bw = bw;
        }

        /**
         * Convenience method used to read messages from the client. Checks the reader if data is available and
         * if so return that data, if no data is available return null.
         *
         * @return any available data or null if no data is available.
         * @throws IOException if there are problems reading from the client.
         */
        public String read () throws IOException {
            if (br.ready()) {
                return br.readLine();
            }
            return null;
        }

        /**
         * Convenience method used to send data to the client, makes sure all data is followed by a newline character and
         * that the buffer is flushed when data has been sent.
         *
         * @param msg the message to send to the client.
         * @throws IOException if there are problems sending data to the client.
         */
        public void write(String msg) throws IOException {
            bw.write(msg);
            bw.newLine();
            bw.flush();
        }
    }
}
