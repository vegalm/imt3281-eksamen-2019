package no.ntnu.imt3281.eksamen;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;
import no.ntnu.imt3281.eksamen.exchange_rates.ExchangeRates;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LineChartController {
    private ExchangeRates exchangeRates;
    private ObservableList<String> currencyData = FXCollections.observableArrayList();
    private final static String url = "https://data.norges-bank.no/api/data/EXR/B..NOK.SP?startPeriod=2019-09-27&endPeriod=2019-10-04&format=sdmx-json&locale=no";
    ObservableList<String> observationDates = FXCollections.observableArrayList();

    @FXML ComboBox comboBoxLineChart;
    @FXML LineChart lineChart;
    @FXML VBox vBox;

    @FXML
    private void initialize(){
        try {
            exchangeRates = new ExchangeRates(new URL(url));
        }catch(IOException io){
            io.printStackTrace();
        }
        for (String s: exchangeRates.getCurrencies()) {
            String tmp = s + " (" + exchangeRates.getCurrencyDescription(s) + ")";
            currencyData.add(tmp);
        }

        comboBoxLineChart.setItems(currencyData);

        for (Date da: exchangeRates.getDates()) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            observationDates.add(format.format(da));
        }

    }

    @FXML
    private void makeLineChart(ActionEvent ae){

        float[] rates = exchangeRates.getExchangeRates(comboBoxLineChart.getSelectionModel().getSelectedItem().toString().substring(0,3));

        NumberAxis yAxis = new NumberAxis();
        CategoryAxis xAxis = new CategoryAxis(observationDates);
        xAxis.setLabel("Dates");
        yAxis.setLabel("Rates");

        XYChart.Series series = new XYChart.Series();

        int lenght = observationDates.size();
        for (int i = 0; i<lenght;i++) {
            series.getData().add(new XYChart.Data(observationDates.get(i), rates[i++]));
        }

        lineChart.getData().clear();
        lineChart.getData().add(series);
    }
}
