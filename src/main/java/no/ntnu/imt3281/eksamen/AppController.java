package no.ntnu.imt3281.eksamen;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import no.ntnu.imt3281.eksamen.exchange_rates.ExchangeRates;

import java.io.IOException;
import java.net.URL;

public class AppController {
    private static final String url = "https://data.norges-bank.no/api/data/EXR/B..NOK.SP?startPeriod=2019-09-27&endPeriod=2019-10-04&format=sdmx-json&locale=no";
    private ObservableList<String> currencyData = FXCollections.observableArrayList();
    private ExchangeRates exchangeRates;

    @FXML TextField enterAmountTextField;
    @FXML TextField answereTextField;
    @FXML ComboBox<String> currencyCB;
    @FXML Label selectItem;
    @FXML VBox vBox;


    @FXML
    private void initialize(){
        selectItem.setVisible(false);
        try {
            exchangeRates = new ExchangeRates(new URL(url));
        }catch(IOException io){
            io.printStackTrace();
        }
        for (String s: exchangeRates.getCurrencies()) {
            String tmp = s + " (" + exchangeRates.getCurrencyDescription(s) + ")";
            currencyData.add(tmp);
        }
        //currencyData.addAll(exchangeRates.getCurrencies());
        currencyCB.setItems(currencyData);
        answereTextField.setEditable(false);
        answereTextField.setDisable(true);

        enterAmountTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    enterAmountTextField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        loadLineChart();
    }

    @FXML
    private void calculateValuta(ActionEvent ae){
        if(ae.getSource()instanceof ComboBox){
            if (enterAmountTextField.getText() == null || enterAmountTextField.getText().trim().isEmpty()) {
                enterAmountTextField.setText("0000");
            }else{
                selectItem.setVisible(false);
                String s = currencyCB.getSelectionModel().getSelectedItem().substring(0,3);
                double dobe = exchangeRates.exchangeTo(Integer.parseInt(enterAmountTextField.getText()), s);
                answereTextField.setText(Double.toString(dobe));
            }
        }
        else{
            if(!currencyCB.getSelectionModel().isEmpty()){
                String s = currencyCB.getSelectionModel().getSelectedItem().substring(0,3);
                double dobe = exchangeRates.exchangeTo(Integer.parseInt(enterAmountTextField.getText()), s);
                answereTextField.setText(Double.toString(dobe));
            }else{
                selectItem.setVisible(true);
            }
        }
    }

    public void loadLineChart(){
        try{
         Pane secPane= FXMLLoader.load(getClass().getResource("/no.ntnu.imt3281.eksamen/LineChart.fxml"));
         vBox.getChildren().add(secPane);
        }catch (IOException io){
            io.printStackTrace();
        }
    }
}
