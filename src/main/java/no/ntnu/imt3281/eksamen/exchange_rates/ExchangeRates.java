package no.ntnu.imt3281.eksamen.exchange_rates;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ExchangeRates {
    private HashMap<Integer, Object> dataSets = new HashMap<Integer, Object>();
    private HashMap<String, Object> currencies = new HashMap<String, Object>();
    private HashMap<Integer, String> observationDates = new HashMap<Integer, String>();
    
    ExchangeRates(String json){
        readJson(json);
    }

    public ExchangeRates(URL url){
        try {
            URLConnection connection = url.openConnection();
            String redirect = connection.getHeaderField("Location");
            if(redirect!=null){
                connection = new URL(redirect).openConnection();
            }
            connection.getContent();

            InputStream is = connection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            String tmp = null;
            while ((tmp=br.readLine())!=null) {
                sb.append(tmp);
            }
            readJson(sb.toString());
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    private void readJson(String json){
        JSONParser parser = new JSONParser();
        try {
            JSONObject obj = (JSONObject) parser.parse(json);
            Iterator<String> iterator = obj.keySet().iterator();
            while(iterator.hasNext()){
                String key = iterator.next();
                if(key.equals("header")){
                    //ubrukelig
                }
                else if(key.equals("dataSets")){
                    JSONObject temp = (JSONObject) ((JSONObject) ((JSONArray) obj.get(key)).get(0)).get("series");
                    Iterator<String> dataSetsIterator = temp.keySet().iterator();

                    while(dataSetsIterator.hasNext()){
                        String dataSetsKey = dataSetsIterator.next();
                        dataSets.put(makeToInteger(dataSetsKey), temp.get(dataSetsKey));
                    }
                }
                else if(key.equals("structure")){
                    JSONObject structure = (JSONObject) obj.get(key);
                    JSONObject dimensions = (JSONObject) structure.get("dimensions");
                    JSONArray series = (JSONArray) dimensions.get("series");
                    JSONObject positionOne = (JSONObject) series.get(1);
                    JSONArray values = (JSONArray) positionOne.get("values");

                    int number = 0;
                    for (Object object: values) {
                        ((JSONObject)object).put("number", number++);
                        String tmp = ((JSONObject)object).get("id").toString();
                        currencies.put(tmp, object);
                    }

                    JSONArray observations = (JSONArray) dimensions.get("observation");
                    JSONObject observationsPositionZero = (JSONObject) observations.get(0);
                    JSONArray valuesObservations = (JSONArray) observationsPositionZero.get("values");
                    int loop = valuesObservations.size();
                    for (int i = 0; i<loop;i++) {
                        observationDates.put(i, ((JSONObject)valuesObservations.get(i)).get("name").toString());
                    }
                }else{
                    System.out.println("Something went wong");
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    boolean currencyExists(String currency) {
        return currencies.containsKey(currency);
    }

    public String getCurrencyDescription(String usd) {
        JSONObject tmp = (JSONObject) currencies.get(usd);
        return  tmp.get("name").toString();
    }

    public Date[] getDates() {
        Date[] dates = new Date[observationDates.size()];
        Iterator<Integer> datePosition = observationDates.keySet().iterator();
        while(datePosition.hasNext()){
            Integer count = datePosition.next();
            try {
                dates[count] = new SimpleDateFormat("yyyy-MM-dd").parse(observationDates.get(count));
            }catch(java.text.ParseException pe){
                pe.printStackTrace();
            }
        }
        return dates;
    }

    public float[] getExchangeRates(String usd) {
        float[] floats = new float[observationDates.size()];
        JSONObject tmp = (JSONObject) dataSets.get(((JSONObject)currencies.get(usd)).get("number"));
        JSONObject rates = (JSONObject) tmp.get("observations");
        Iterator<String> findFloatIterator = rates.keySet().iterator();
        while(findFloatIterator.hasNext()){
            String key = findFloatIterator.next();
            floats[Integer.parseInt(key)] = Float.parseFloat(((JSONArray)rates.get(key)).get(0).toString());
        }
        return floats;
    }

    public double exchangeTo(int i, String usd) {
        float rate = (float) getExchangeRates(usd)[observationDates.size()-1];
        double money = i/rate;
        if(hasAttributeThree(usd)) money *= 100;

        return money;
    }

    double exchangeFrom(int i, String usd) {
        float rate = (float)getExchangeRates(usd)[observationDates.size()-1];
        double money = i*rate;
        if(hasAttributeThree(usd))money/=100;
        return money;
    }

    public String[] getCurrencies() {
        String[] allRates = new String[currencies.size()];
        Iterator<String> iterator = currencies.keySet().iterator();
        while(iterator.hasNext()){
            String key = iterator.next();
            allRates[Integer.parseInt(((JSONObject)currencies.get(key)).get("number").toString())] = key;
        }
        return allRates;
    }

    private int makeToInteger(String s){
        return Integer.parseInt(s.split(":")[1]);
    }

    private boolean hasAttributeThree(String s){
        JSONObject tmp = (JSONObject) dataSets.get(((JSONObject)currencies.get(s)).get("number"));
        JSONArray rates = (JSONArray) tmp.get("attributes");
        return ((Long)rates.get(2) >0);
    }
}
